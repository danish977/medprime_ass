from flask import Flask, request, jsonify, render_template, redirect
from collections import Counter
import json
import os
import uuid
import re

basedir = os.path.abspath(os.path.dirname(__file__))
app = Flask(__name__)
app.config.update(
    UPLOADED_PATH = os.path.join(basedir, 'file_upload')
)


@app.route('/')
def fortend():
    return render_template('index.html')

@app.route('/wordcount')
def wordcount():
    return render_template('word_count.html')

@app.route('/textfile-upload', methods = ['POST'])
def wordcount_logic():

    uploaded_files = request.files['text_file']
    uplaod_dir_file = custom_filename_uplaod(uploaded_files)
    uploaded_files.save(uplaod_dir_file)
    count = {}
    with open(uplaod_dir_file) as f:
        contents = f.read().strip().lower()
        item = ["this", "there", "the"]
        for x in item:
            count[x] = sum(1 for match in re.finditer(rf"\b{x}\b", contents))
            
    return count
        
    

def custom_filename_uplaod(file):
    file.filename = str(uuid.uuid4())+'.txt'
    upload_path = os.path.join(app.config['UPLOADED_PATH'],file.filename)
    return upload_path


if __name__ == '__main__':
    app.run(debug=True)